house.raw<-read.csv("kc_house_data.csv")
house.copy<-house.raw
str(house.copy)

# Task 2 :
#���� �� �� ����� ������ �����, �� �� ����� �� ������� ���� ������� ����� 
func1<-function(x){
  if(any(is.na(x)))
   x<-na.omit(x)
  return(x)
}
  
house.copy<-func1(house.copy)
house.copy

# Task 3 :
#EDA (Exploratory Data Analysis)
install.packages('ggplot2')
library(ggplot2)

#����� �������
house.copy$date<-substring(house.copy$date,1,8)
house.copy$date<-as.Date(house.copy$date,"%Y%m%d")
house.copy$waterfront<-as.factor(house.copy$waterfront)
house.copy$floors<-as.factor(house.copy$floors)
house.copy$condition<-as.factor(house.copy$condition)
house.copy$yr_built<-as.factor(house.copy$yr_built)

# ����� ������� ���������
summary(house.copy$price)
price_cut<-c(0,75001,250000,400000,800000,1000000,3000000,7000000,20000000)
labels<- c("up to 75K","75k-250K","250k-400k","400k-800k","800k-1m","1m-3m","3m-7m","7m-20m")

# bin the data
#cut ����� �� ������� ��������
bins<-cut(x= house.copy$price,breaks = price_cut,labels = labels,include.lowest = TRUE,right = FALSE)

# ��������� �� ����� ������
ggplot(house.copy,aes(price))+geom_histogram(fill="lightblue",col="black")+
  labs(title= "Histogram for Price")

#����� ��� ���� ����� ����� �������?
day<-substring(house.copy$date,9,10)
house.copy$day<-as.numeric(day)
ggplot(house.copy,aes(day))+geom_histogram(fill="lightblue",col="black")+
  labs(title= "Histogram for day sale")+ scale_x_continuous(breaks = seq(0,31))

#����� ��� ���� ���� ���� ��� ��� ���� ���� (���� �� ���������
ggplot(house.copy,aes(x=view,y=price))+geom_bar(stat = "identity",width = 0.7, fill="lightblue")

#������ ���� ������� ������ ��� ����� ����
agg1 <- aggregate(house.copy$price, by=list(house.copy$grade), median)
names(agg1) <- c("Grade","Median_Price")

ggplot(agg1, aes(Grade, Median_Price))+
geom_bar(stat = "identity", colour="black", fill = "lightblue")+scale_x_continuous(breaks = seq(0,13))

#������� ����� ������
library(scales)
ggplot(house.copy, aes(price)) + geom_density()  +
scale_x_continuous(labels = comma, limits = c(0, 2e+06)) +xlab("price")+ ggtitle("Price distribution")

# ��� ����� ��� ��� ������� ����� ���� ���� ���� ����
ggplot( house.copy, aes(bins, house.copy$bedrooms)) +geom_boxplot() 

#��� ��� ����� ���� ����� �� ���� ������
year<-substring(house.copy$date,1,4)
house.copy$year<-as.numeric(year)
ggplot(data = house.copy, aes(as.factor(year), price)) + geom_boxplot()

# Task 4
install.packages('caTools')
library('caTools')
house.copy$id<-NULL
house.copy$zipcode<-NULL
house.copy$lat<-NULL
house.copy$long<-NULL
house.copy$sqft_lot<-NULL
house.copy$sqft_above<-NULL
house.copy$sqft_basement<-NULL
str(house.copy)

filter<- sample.split(house.copy$price,SplitRatio = 0.7)
house.train<-subset(house.copy,filter==T)
house.test<-subset(house.copy,filter==F)

#������ �� ����� 
dim(house.copy)
dim(house.train)
dim(house.test)

#����� �������� ������
model<-lm(price~.,house.train)
summary(model)

#����� ��� ������ ����� �������
predicted.train<-predict(model,house.train)
predicted.test<-predict(model,house.test)
MSE.train<- mean((house.train$price-predicted.train)**2)
MSE.test<- mean((house.train$price-predicted.test)**2)
MSE.train

MSE.train**0.5
MSE.test**0.5
#�� ��� ���� ��� ������ ������� ���� �� ���� ������ �������� �� ����� ����� ��"� ,�� ����� ����� ��� ��� �������
mean(house.copy$price)
